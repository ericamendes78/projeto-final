package com.itau.conta.controllers;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.conta.controllers.ContaController;
import com.itau.conta.models.Conta;
import com.itau.conta.repositories.IContaRepository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(ContaController.class)
public class ContaControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	IContaRepository contaRepository;	
	
	ObjectMapper mapper = new ObjectMapper();

	@Test
	public void DeveRetornarContas() throws Exception{
		Conta conta = new Conta();
		
		conta.setId(1);

		ArrayList<Conta> contas = new ArrayList<Conta>();
		contas.add(conta);
		
		when(contaRepository.findAll()).thenReturn(contas);
		
		mockMvc.perform(get("/contas"))
		.andExpect(jsonPath("$[0].id", equalTo(1)))
		.andExpect(jsonPath("$[0].agencia", equalTo("1234")));
	}
	
	@Test
	public void DeveRetornarContasPorQuantidade() throws Exception{
		Conta conta = new Conta();
		
		conta.setId(1);

		ArrayList<Conta> questoes = new ArrayList<Conta>();
		questoes.add(conta);
		
		when(contaRepository.findAll()).thenReturn(questoes);
		
		mockMvc.perform(get("/questoes/1"))
		.andExpect(jsonPath("$[0].id", equalTo(1)))
		.andExpect(jsonPath("$[0].pergunta", equalTo("Voce trabalha?")));
	}
	
	@Test
	public void DeveRetornarContaUm() throws Exception{
		Conta conta = new Conta();
		
		conta.setId(1);
		
		Optional<Conta> contaOptional = Optional.of(conta);

		
		when(contaRepository.findById(conta.getId())).thenReturn(contaOptional);
		
		mockMvc.perform(get("/conta/1"))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.pergunta", equalTo("Voce trabalha?")));
	}
	
	@Test
	public void DeveRetornarContaInexistente() throws Exception{
		Conta conta = new Conta();
		conta.setId(1);
		
		Optional<Conta> contaOptional = Optional.empty();

		
		when(contaRepository.findById(conta.getId())).thenReturn(contaOptional);
		
		mockMvc.perform(get("/conta/1"))
		 .andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	
	@Test
	public void DeveRetornarContaCriada() throws Exception{
		
		Conta conta = new Conta();
		
		conta.setId(1);
		
		when(contaRepository.save(any(Conta.class))).thenReturn(conta);
		
		String json = mapper.writeValueAsString(conta);

		mockMvc.perform(post("/conta")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.pergunta", equalTo("Voce trabalha?")));
	}	
	
	@Test
	public void DeveDeletarConta() throws Exception{
		Conta conta = new Conta();
		
		conta.setId(1);

		Optional<Conta> contaOptional = Optional.of(conta);
				
		when(contaRepository.findById(conta.getId())).thenReturn(contaOptional);
		contaRepository.deleteById(conta.getId());
		
		mockMvc.perform(delete("/conta/1"))
		 .andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void DeveDeletarContaInexistente() throws Exception{
		Conta conta = new Conta();
		conta.setId(1);

		Optional<Conta> contaOptional = Optional.empty();
				
		when(contaRepository.findById(conta.getId())).thenReturn(contaOptional);
		
		mockMvc.perform(delete("/conta/1"))
		 .andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	@Test
	public void DeveAlterarConta() throws Exception{
		
		Conta conta = new Conta();
		
		conta.setId(1);
		
		Optional<Conta> contaOptional = Optional.of(conta);
				
		when(contaRepository.findById(conta.getId())).thenReturn(contaOptional);
		when(contaRepository.save(any(Conta.class))).thenReturn(conta);
		
		String json = mapper.writeValueAsString(conta);

		mockMvc.perform(put("/conta/1")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.pergunta", equalTo("Voce trabalha?")));
	}
	
}
