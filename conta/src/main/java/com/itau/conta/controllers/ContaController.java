package com.itau.conta.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.conta.models.Conta;
import com.itau.conta.repositories.IContaRepository;

@RestController
public class ContaController {
	
	@Autowired
	IContaRepository contaRepository;
	
	@RequestMapping(method=RequestMethod.POST,path="/conta/abrir")
	public Conta abrirConta(@Valid @RequestBody Conta conta){
		return contaRepository.save(conta);
	}
	
	@RequestMapping(method=RequestMethod.GET,path="/contas")
	public Iterable<Conta> buscarContas(){
		return contaRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET,path="/conta/{id}")
	public ResponseEntity<?> buscarConta(@PathVariable long id){
		
		Optional<Conta> contaOptional  = contaRepository.findById(id);
		
		if(!contaOptional.isPresent()){
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(contaOptional.get());
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/conta/desativar/{id}")
	public ResponseEntity<?> desativarConta(@PathVariable long id){
		
		Optional<Conta> contaOptional = contaRepository.findById(id);
		
		if(!contaOptional.isPresent()){
			return ResponseEntity.notFound().build();
		}
		
		Conta conta = contaOptional.get();
		conta.setSaldo(0.00);
		conta.setAtiva(false);
		
		Conta contaBanco = contaRepository.save(conta);
		
		return ResponseEntity.ok().body(contaBanco);
	}
	
//	@RequestMapping(method=RequestMethod.POST, path="/conta/transferir/")
//	public ResponseEntity<?> transferir(@Valid @RequestBody Conta contaOrigem,@Valid @RequestBody Conta contaDestino, double valor){
//		
//		Optional<Conta> contaOrigemOptional =  contaRepository.findById(contaOrigem.getId());
//		if(!contaOrigemOptional.isPresent()){
//			return ResponseEntity.notFound().build();
//		}
//		
//		Optional<Conta> contaDestinoOptional =  contaRepository.findById(contaDestino.getId());
//		if(!contaDestinoOptional.isPresent()){
//			return ResponseEntity.notFound().build();
//		}
//		
//		 Conta.transferir(contaOrigemOptional.get(),contaDestinoOptional.get(), valor);
//		 
//		 contaRepository.save(contaOrigemOptional.get());
//		 contaRepository.save(contaDestinoOptional.get());
//
//		return ResponseEntity.ok().build();
//	}
//	
	
	
}
