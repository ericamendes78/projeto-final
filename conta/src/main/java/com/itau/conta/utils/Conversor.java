package com.itau.conta.utils;

import org.json.JSONObject;
import com.google.gson.Gson;

public class Conversor {

	// static JSONObject json = new JSONObject();
	static Gson gson = new Gson();

	public static String converterGSON(Object obj) {
		return gson.toJson(obj).toString();
	}

}
