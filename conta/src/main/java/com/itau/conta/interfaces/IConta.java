package com.itau.conta.interfaces;

import java.util.ArrayList;

import com.itau.conta.models.Conta;

public interface IConta {
	
	public boolean abrir();
	public boolean desativar();
	public boolean transferir(Conta conta1, Conta conta2, double valor);
	public boolean depositar(Conta conta1, Conta conta2, double valor);
	public double saldo();
	public ArrayList<String> extrato();
	public ArrayList<String> extrato(int dias);
}
