package com.itau.conta.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.conta.models.Conta;

public interface IContaRepository extends CrudRepository<Conta, Long> {

}
