package com.itau.conta.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;

import com.itau.conta.utils.Conversor;

@Entity
public class Correntista {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	@Size(min=3,max=100)
	private String nome;
	
	@NotNull
	private long cpf;
	
	@NotNull
	private Date dataNasc;
	
	public Correntista(String nome, long cpf, Date dataNasc){
		setNome(nome);
		setCpf(cpf);
		setDataNasc(dataNasc);
	}
	
	public Correntista(String nome,long cpf){
		this(nome, cpf, null);
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public long getCpf() {
		return cpf;
	}
	public void setCpf(long cpf) {
		this.cpf = cpf;
	}
	public Date getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}
	
	public String toString(){
		return Conversor.converterGSON(this);
	}
	
}
