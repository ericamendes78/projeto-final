package com.itau.conta.models;

import java.util.Date;

import com.itau.conta.utils.Conversor;

public class ContaPoupanca extends Conta {
	
	private int diaAniversario;
	private double taxaRendimento;
		
	public ContaPoupanca(Correntista titular, long agencia, double saldo) {
		super(titular, agencia, saldo);
		setTaxaRendimento(0.06);
	}
	
	public int getDiaAniversario() {
		return diaAniversario;
	}

	public void setDiaAniversario(int dataAniversario) {
		this.diaAniversario = dataAniversario;
	}

	public double getTaxaRendimento() {
		return taxaRendimento;
	}

	public void setTaxaRendimento(double taxaRendimento) {
		this.taxaRendimento = taxaRendimento;
	}
	
	public double rendimentoMensal(){
		return (getTaxaRendimento() * getSaldo()) / 100;
	}
	
	public double saldoComRendimento(){
		Date dataAtual = new Date(System.currentTimeMillis());  
		
		//Aplica o rendimento somente se chegou na data da ultima aplicacao
		if(dataAtual.getDay() == getDiaAniversario()){
			 setSaldo(getSaldo() + rendimentoMensal());
		}
		 return getSaldo();
	}
	
	public void aplicar(double valor){
		if(isAtiva()){
			setSaldo(getSaldo() + valor);
		}
	}
	
	public void resgatar(double valor){
		if(isAtiva() && getSaldo() >= valor){
			setSaldo(getSaldo() - valor);
		}
	}
	
}
