package com.itau.conta.models;

import java.util.Date;
import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;

import com.itau.conta.utils.Conversor;

@Entity
public class Conta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	@Size(max=4)
	private long agencia;
	
	@NotNull
	@Size(max=6)
	private long conta;
	
	@NotNull
	private long digito;

	private double saldo;
	private Date dataCriacao;
	private Date dataDesativacao;
	private boolean ativa;
	
	@NotNull
	private Correntista correntista;
	
	public Conta(){
		
	}
	
	public Conta(Correntista titular, long agencia){
		this(titular, agencia, 0.00);
	}
		
	public Conta(Correntista titular,long agencia, double saldo){
		setCorrentista(titular);
		setAgencia(agencia);
		setSaldo(saldo);
		
		Random gerador = new Random();		
		setConta(gerador.nextInt(90000));
		
		setDigito(gerador.nextInt(9));
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getAgencia() {
		return agencia;
	}
	
	public void setAgencia(long agencia) {
		this.agencia = agencia;
	}
	
	public long getConta() {
		return conta;
	}
	
	public void setConta(long conta) {
		this.conta = conta;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataDesativacao() {
		return dataDesativacao;
	}

	public void setDataDesativacao(Date dataDesativacao) {
		this.dataDesativacao = dataDesativacao;
	}

	public boolean isAtiva() {
		return ativa;
	}

	public void setAtiva(boolean status) {
		this.ativa = status;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public long getDigito() {
		return digito;
	}

	public void setDigito(long digito) {
		this.digito = digito;
	}

	public Correntista getCorrentista() {
		return correntista;
	}

	public void setCorrentista(Correntista correntista) {
		this.correntista = correntista;
	}

	public static void transferir(Conta contaOrigem, Conta contaDestino, double valor){
		boolean permiteTransferir = contaOrigem.isAtiva() && contaOrigem.getSaldo() > 0;
		if(permiteTransferir && contaDestino.isAtiva()){
			contaOrigem.setSaldo(contaOrigem.getSaldo() - valor);
			contaDestino.setSaldo(contaDestino.getSaldo() + valor);
		}
	}
	
	public static void depositar(Conta conta, double valor){
		if(conta.isAtiva()){
			conta.setSaldo(conta.getSaldo() + valor);	
		}
	}
	
	public String toString(){
		return Conversor.converterGSON(this);
	}
		
}
