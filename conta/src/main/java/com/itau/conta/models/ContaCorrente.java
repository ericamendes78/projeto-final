package com.itau.conta.models;

public class ContaCorrente extends Conta {
	
	private double limite;

	public ContaCorrente(Correntista cliente, long agencia, double saldo) {
		super(cliente, agencia,saldo);
		setLimite(800);
	}

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}
	
}
